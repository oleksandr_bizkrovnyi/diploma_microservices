#!/bin/sh

set -e

host="discovery"
port="8761"
cmd="java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"

>&2 echo "!!!!!!!! Check discovery for available !!!!!!!!"

until curl http://"$host":"$port"; do
#until busybox-extras telnet localhost 9092; do
  >&2 echo "discovery is unavailable - sleeping"
  sleep 1
done

>&2 echo "discovery is up - executing command"

exec $cmd

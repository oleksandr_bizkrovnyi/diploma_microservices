#!/bin/sh

set -e

host="config"
port="8888"
cmd="java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"

>&2 echo "!!!!!!!! Check config server for available !!!!!!!!"

until curl http://"$host":"$port"; do
#until busybox-extras telnet localhost 8888; do
  >&2 echo "config server is unavailable - sleeping"
  sleep 1
done

>&2 echo "config server is up - executing command"

exec $cmd

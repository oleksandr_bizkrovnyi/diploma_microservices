package com.bizkrovnyi.user.service.service;

import com.bizkrovnyi.user.service.domain.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    Long createUser(User user);
}

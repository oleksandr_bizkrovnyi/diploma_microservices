package com.bizkrovnyi.user.service.domain;

import lombok.Data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "USERS")
@Data
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "USER_EMAIL")
    @NotBlank
    private String email;
}

package com.bizkrovnyi.user.service;

import com.bizkrovnyi.user.service.domain.User;
import com.bizkrovnyi.user.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
public class Controller {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/all")
    public ResponseEntity getAll() {
        User user = new User();
        user.setId(123123L);
        user.setEmail("emails");
        return new ResponseEntity<>(Stream.of(user).toArray(), HttpStatus.OK);
    }


    @PostMapping(path = "/save")
    public ResponseEntity save(@RequestBody User user) {
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
    }
}

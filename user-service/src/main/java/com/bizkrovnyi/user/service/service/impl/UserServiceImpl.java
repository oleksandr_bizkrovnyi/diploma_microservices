package com.bizkrovnyi.user.service.service.impl;

import com.bizkrovnyi.user.service.domain.User;
import com.bizkrovnyi.user.service.repository.UserRepository;
import com.bizkrovnyi.user.service.sender.Sender;
import com.bizkrovnyi.user.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Value("${spring.kafka.topic.userCreated}")
    public String USER_CREATED_TOPIC;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private Sender sender;


    @Override
    public List<User> getAll() {sender.send(USER_CREATED_TOPIC, new User());

        return userRepository.findAll();
    }

    @Override
    public Long createUser(User user) {
        userRepository.save(user);
        sender.send(USER_CREATED_TOPIC,user);
        return user.getId();
    }
}

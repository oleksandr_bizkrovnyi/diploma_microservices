package com.bizkrovnyi.user.service.repository;

import com.bizkrovnyi.user.service.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}

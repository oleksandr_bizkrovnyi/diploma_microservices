package com.bizkrovnyi.user.service.receiver;

import com.bizkrovnyi.user.service.domain.User;
import lombok.extern.java.Log;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Log
@Component
public class Receiver {

    private CountDownLatch latch = new CountDownLatch(1);

    @KafkaListener(topics = "${spring.kafka.topic.userCreated}")
    public void receive(User payload) {
        log.info(payload.getId().toString());
        latch.countDown();
    }
}
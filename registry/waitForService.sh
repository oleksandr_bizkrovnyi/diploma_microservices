#!/bin/sh

set -e

cmd="java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar"

>&2 echo "!!!!!!!! Check kafka for available !!!!!!!!"

until busybox-extras telnet kafka1 19092; do
  >&2 echo "Kafka is unavailable - sleeping"
  sleep 1
done

>&2 echo "Kafka is up - executing command"

exec $cmd
